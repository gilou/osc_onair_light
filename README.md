# OSC_OnAir_Light

Try to light on an "On Air" sign, when a DCA is unmuted on an OSC console.

## What is that thing?

A single script for now for a single usage, but may be cleaned up 
and made to be more generic as needed.

Feel free to interact, but I probably won't make it do things I don't 
actually need for now, unless it draws enough attentions and other devs.

## What does it do, for now

Basic script to connect and listen for OSC events from a Behringer X32, and 
react when a DCA mute is toggled, to turn on or off an on-air sign.
Should work with any OSC capable console (Behringer's XR / X / M)
toggles rather roughly a RFR3 Sonoff relay for now.

## Usage

No configuration for now, so setup variables for X32 IP address, RFR3.
Use `pip install -r requirements.txt` to install dependencies:
  - oscpy: for OSC interaction (because it allows to deal with sockets
    which is required by the X32, which answers on the source port of
    the request, MIT licence
  - requests: rather raw interaction with RFR3's HTTP JSON API.
    Apache Software licence 2.0

Then run `./x32_osc_dca_relay.py`

## Author, Licence

License: GNU Affero General Public License v3, see [COPYING](COPYING)
Author: Gilles Pietri <contact+dev@gilouweb.com>

