#! /usr/bin/env python3
"""OSC_OnAir_Light: Try to light on an "On Air" sign, when a DCA is unmuted
Basic script to connect and listen for OSC events from a Behringer X32, and 
react when a DCA mute is toggled, to turn on or off an on-air sign.

Copyright © 2023 Gilles Pietri <contact+dev@gilouweb.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""
from oscpy.server import OSCThreadServer
from time import sleep
import requests

x32_address = "192.168.2.32"
x32_port = 10023

rfr3_ip = "192.168.27.30"
rfr3_port = 8081


def default_handler(address, *values):
    pass


osc = OSCThreadServer(default_handler=default_handler, advanced_matching=True)
sock = osc.listen(address="192.168.2.5", port=10023, default=True)

dca_state = [0, 0]


def on_air():
    if dca_state == [0, 0]:
        print("OFF AIR")
        sw = "off"
    else:
        sw = "on"
        print("ON AIR")
    data = {"data": {"switch": sw}, "deviceid": ""}
    rep = requests.post(f"http://{rfr3_ip}:{rfr3_port}/zeroconf/switch", json=data)


@osc.address(b"/info")
def callback(*values):
    print("got values: {}".format(values))


@osc.address(b"/dca/[1-2]/on", get_address=True)
def dca_mutes(address, *values):
    print("got dca mute info from address {}, value  {}".format(address, values[0]))
    is_on = values[0]
    if b"/1/" in address:
        dca_state[0] = is_on
    elif b"/2/" in address:
        dca_state[1] = is_on
    on_air()


osc.send_message(b"/info", [], x32_address, x32_port)
for i in [1, 2]:
    msg = f"/dca/{i}/on"
    osc.send_message(str.encode(msg), [], x32_address, x32_port)

while True:
    osc.send_message(b"/xremote", [b"/dca/[1-2]/on"], x32_address, x32_port)
    sleep(10.0)
